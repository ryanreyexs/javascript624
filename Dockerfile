FROM debian:latest
RUN apt update
RUN apt install nodejs npm -y
COPY ./web/ /app/
WORKDIR /app/
RUN npm install express
RUN npm install mysql
EXPOSE 8005
CMD node index.js
